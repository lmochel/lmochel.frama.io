# Site web personnel

*Repris de [Francesca Guiducci](https://github.com/engfragui/personal-website)*

https://lmochel.frama.io/

Page web personnelle minimale générée avec Python et [Flask](http://flask.pocoo.org/).
Le déploiement (à venir) est pris en charge par [Frozen-Flask](https://pythonhosted.org/Frozen-Flask/) et [Gitlab Pages](https://docs.framasoft.org/fr/gitlab/gitlab-pages.html).

Idée pour la suite ? => https://tylermclaughlin.github.io

## Développement

En cas d'installation de nouveau package, se souvenir de mettre à jour le fichier `requirements.txt`:

```sh
pip freeze > requirements.txt
```

## Déploiement (à modifier)

<a href="https://www.netlify.com">
  <img src="https://www.netlify.com/img/global/badges/netlify-dark.svg"/>
</a>

### Netlify settings

* Build command: `python freeze.py`
* Publish directory: `build`

## Pour aller plus loin

*   [Créer un site statique minimaliste avec Flask et Frozen Flask](https://blog.david-dahan.com/cr%C3%A9er-un-site-statique-minimaliste-avec-flask-et-frozen-flask-80d5c741ccbe)
*   [Static Websites with Frozen Flask](https://john-b-yang.github.io/flask-website/)

*   [Create and Host your Personal Website for Almost Nothing — Pt. 1 Flask’n’Freeze](https://medium.com/@aclaytonscott/create-and-host-your-personal-website-for-almost-nothing-pt-1-flaskn-freeze-2a8f7f808a4)
*   [Create a static blog using Python Flask](https://dev.to/arrantate/create-a-static-blog-using-python-flask-1oab#hosting-on-githubio)
